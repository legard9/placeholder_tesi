import json
import math
from matplotlib import cm
from scipy.ndimage.interpolation import rotate
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.rcParams.update({'text.color' : "white",
                     'axes.labelcolor' : "white",
                     "ytick.color" : "w",
                     "xtick.color" : "w",
                     "axes.labelcolor" : "w",
                     "axes.edgecolor" : "w",
                     'axes.facecolor' : '#37474F'})

np.set_printoptions(suppress=True, precision=3)
pd.set_option('display.float_format', lambda x: '%.3f' % x)
np.set_printoptions(linewidth=5000)
pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)


def main():
    files = ["ControFlusso", "DoppiaSceltaConAgentiSottoSopra", "RelazioneProf230x30Agenti",
             "RelazioneProf340x40MoltiAgenti", "RelazioneProfConAgenti", "TriplaSceltaConAgenti", "TriplaSceltaFacile"]
    i = 0
    for file in files:

        """Creazione dei due DataFrame RL e NavMesh"""
        dfFile1 = pd.read_csv(f"Files/TestFinale/PythonPosPath{file}(Clone).txt", decimal=",",
                              delimiter=";",
                              header=None, names=["posX", "posZ", "speed", "colore", "ID", "desiredSpe", "time", "type"])
        dfFile2 = pd.read_csv(f"Files/TestFinale/PythonPosPath{file}NMA(Clone).txt",
                              decimal=",", delimiter=";",
                              header=None, names=["posX", "posZ", "speed", "colore", "ID", "desiredSpe", "time", "type"])

        dfFile1["type"] = 0
        dfFile2["type"] = 1
        dfFile1["colore"] = "red"
        dfFile2["colore"] = "white"
        dfFile1["ID"] = dfFile1["ID"] + 1000
        # print(dfFile2.describe())
        # print(dfFile1.describe())

        """Concatenazione dei due dataframe"""
        dfStartPos = pd.concat([dfFile1, dfFile2], ignore_index=True)

        print(dfStartPos)
        print(dfStartPos.describe())

        if file == "RelazioneProf230x30Agenti":
            listaAree = [
                [-14.5, 14.5, -14.5, 14.5],
            ]
        elif file == "RelazioneProf340x40MoltiAgenti":
            listaAree = [
                [-19.5, 19.5, -19.5, 19.5],
            ]
        else:
            listaAree = [
                [-9.5, 9.5, -9.5, 9.5],
            ]
        i = i + 1
        for n, area in enumerate(listaAree):
            """Creazione del dataframe per le determinate aree"""
            xa, xb, ya, yb = area
            dfPosizioni = dfStartPos.copy()
            dfPosizioni = dfPosizioni[xa <= dfStartPos["posX"]]
            dfPosizioni = dfPosizioni[dfPosizioni["posX"] <= xb]
            dfPosizioni = dfPosizioni[ya <= dfPosizioni["posZ"]]
            dfPosizioni = dfPosizioni[dfPosizioni["posZ"] <= yb]
            superficie = (area[1] - area[0]) * (area[3] - area[2])

            grafici = 3

            """Richiamo delle funzioni per la creazione dei grafici"""
            _ = plt.figure(1 + grafici * i)
            velr_veld(dfPosizioni)
            plt.savefig(f"Plot/TestFinale/{file}/{file}VelDes.png")
            _ = plt.figure(2 + grafici * i)
            img = plt.imread(f"Sfondi/{file}.png")
            plt.imshow(img, extent=area)
            lineaPercorso(dfPosizioni)
            plt.savefig(f"Plot/TestFinale/{file}/{file}Traiettorie.png")
            _ = plt.figure(3 + grafici * i)
            tempoPercorrenza(dfFile1["time"].values, dfFile2["time"].values)
            plt.savefig(f"Plot/TestFinale/{file}/{file}BoxPlot.png")


    return


def tempoPercorrenza(rlTempi, navTempi):
    """Funzione per la creazione dei box-plot riguardanti i tempi di percorrenza

            Arguments:
                rlTempi -- Tempi di percorrenza dell'agente Reinforcement Learning
                navTempi -- Tempi di percorrenza dell'agente NavMesh

            Returns:
                Crea il grafico contenente i due box-plot dei tempi di percorrenza, il primo dell'agente RL,
                il secondo dell'agente NavMesh

            """

    plt.boxplot([rlTempi, navTempi])
    plt.suptitle("Agents' travel time RL vs NAV")
    plt.xlabel("Type")
    plt.ylabel('T [s]')
    plt.xticks([1, 2], ["RL", "NAV"])


def lineaPercorso(dfPosizioni):
    """Funzione per la creazione del grafico riguardante le traiettorie

            Arguments:
                dfPosizioni -- DataFrame contenente i dati degli agenti nell'area da analizzare

            Returns:
                Crea il grafico contenente le traiettorie degli agenti RL e dell'agente NavMesh.

            """

    df = dfPosizioni.copy()

    a = True
    b = True
    for id in df.groupby(["ID"]).count().index:
        """Prendo i valori delle posizioni degli agenti"""
        X = df[df["ID"] == id]["posX"].values.tolist()
        Z = df[df["ID"] == id]["posZ"].values.tolist()
        col = df[df["ID"] == id]["colore"].values.tolist()[0]
        """Serie di if-Else per non buggare la legenda e per mettere in primo piano il colore dell'agente NavMesh"""
        if (col == "white") & a:
            plt.plot(X, Z, color=col, zorder=2, label="NavMesh")
            a = False
        elif col == "white":
            plt.plot(X, Z, color=col, zorder=2)
        if (col == "red") & b:
            plt.plot(X, Z, color=col, zorder=1, label="RL")
            b = False
        elif col == "red":
            plt.plot(X, Z, color=col, zorder=1)
    plt.gca().legend(loc="best")
    plt.suptitle("Agents' path RL vs NAV")
    plt.xlabel('X [m]')
    plt.ylabel('Z [m]')


def velr_veld(dfPosizioni):
    """Funzione per la creazione dell'istogramma con velocità desiderate e reali

            Arguments:
                dfPosizioni -- DataFrame contenente i dati degli agenti nell'area da analizzare

            Returns:
                Crea l'istogramma relativo al paragone tra le velocità desiderate e le velocità reali

            """

    df = dfPosizioni.copy()
    df = df.groupby(['ID']).mean()

    pesi = np.ones(len(df["speed"])) / len(df["speed"])

    bins = np.arange(0, 1.8, 0.05)
    # print(bins)

    plt.hist(df["speed"], color=[(0, 0, 1, 0.5)], weights=pesi, bins=bins)
    plt.hist(df["desiredSpe"], color=[(1, 0, 0, 0.5)], weights=pesi, bins=bins)
    plt.gca().legend(('Actual speed', 'Desired speed'), loc="upper left")
    plt.suptitle("Comparison between Actual speed and Desired speed")
    plt.xlabel('Speed [m/s]')
    plt.ylabel('% of agents')


def tensor():
    """Funzione per la creazione del grafico riguardante l'andamento della reward nel tempo e per ogni ambiente

            Returns:
                Crea il grafico contenente l'andamento della reward

            """

    dfTensor = pd.read_csv(f"reward.csv")
    dfTensor2 = pd.read_csv(f"group.csv")
    dfFlags = pd.read_csv(f"EnvironmentChanges.txt", decimal=",", delimiter=";", header=None,
                          names=["Data", "Wall time", "Nome"])
    elencoStepsCambio = []
    """Per gestire i cambiamenti di ambiente"""
    for i in range(len(dfFlags)):
        cambio = dfFlags["Wall time"].iloc[i]
        for j in range(len(dfTensor) - 1):
            time0 = dfTensor["Wall time"].iloc[j]
            time1 = dfTensor["Wall time"].iloc[j + 1]

            if time0 <= cambio < time1:
                step0 = dfTensor["Step"].iloc[j]
                step1 = dfTensor["Step"].iloc[j + 1]
                percentuale = (cambio - time0) / (time1 - time0)

                stepCambio = percentuale * (step1 - step0) + step0
                elencoStepsCambio.append(stepCambio)
                break

    from scipy.ndimage.filters import gaussian_filter1d
    stepSmoothed1 = gaussian_filter1d(dfTensor["Step"], sigma=2)
    valueSmoothed1 = gaussian_filter1d(dfTensor["Value"], sigma=2)
    stepSmoothed2 = gaussian_filter1d(dfTensor2["Step"], sigma=2)
    valueSmoothed2 = gaussian_filter1d(dfTensor2["Value"], sigma=2)
    plt.plot(stepSmoothed1, valueSmoothed1, color = "white")
    plt.plot(stepSmoothed2, valueSmoothed2, color = "black")

    step_prima = 0
    textSpace = -10000000
    rewardMin = -80
    rewardMax = 20
    """Per la creazione del grafico"""
    for n, temp_step in enumerate(elencoStepsCambio):
        plt.fill_between([step_prima, temp_step], rewardMin, rewardMax, alpha=0.4)
        #plt.fill_between([step_prima, temp_step], 30, 70, color="white")
        plt.plot([temp_step, temp_step], [rewardMin, rewardMax])
        nome = dfFlags["Nome"].iloc[n]
        # nome = nome[4:len(nome)]
        textSpaceBef = textSpace
        textSpace = (temp_step - step_prima) / 2 + step_prima
        val = 10000
        if (textSpace - textSpaceBef) < val:
            textSpace = textSpace + val / 2
        print(textSpace)
        plt.text(textSpace, -78, nome, ha="center", fontsize=13, rotation=270)
        step_prima = temp_step

    plt.fill_between([step_prima, dfTensor["Step"].iloc[-1]], rewardMin, rewardMax, alpha=0.4)
    plt.text((dfTensor["Step"].iloc[-1] - step_prima) / 2 + step_prima, -78, "Retrain/Consolidation", ha="center", fontsize=13,
             rotation=270)

    plt.suptitle("Cumulative reward during training")
    plt.xlabel('Step')
    plt.ylabel('Cumulative Reward')

if __name__ == '__main__':
    _ = plt.figure(1000,facecolor='#37474F')
    tensor()
    #main()
    plt.show()
